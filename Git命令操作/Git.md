### Git

1. 生成公钥私钥: ssh-keygen -t rsa -C "L_Wayne@163.com"
2. 全局配置用户名和邮箱(非全局配置删除: --global): 
   1. git config --global user.name L_Wayne
   2. git config --global user.email L_Wayne@163.com
3. 添加git远程仓库地址: git remote add origin git@git、、、
4. 从远程仓库获取所有分支: git fetch origin
5. 切换分支: git checkout -b 分支名 origin/分支名
6. 下拉文件: git pull origin master
7. 将所有文件添加到暂存区: git add . 
8. 将暂存区文件推送到本地仓库: git commit -m 'log'
9. 将本地仓库文件推送到远程仓库: git push origin master
10. 查看状态: git status
11. 本地关联远程分支
    1. git branch --set-upstream-to=origin/master master
12. 分支
    1. git branch 查看本地分支
    2. git branch -a 查看远程分支

