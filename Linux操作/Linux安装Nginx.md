### Linux安装Nginx

> ##### Nginx是一个Http服务器，它能反向代理HTTP、HTTPS、SMTP、POP3、IMAP的协议连接，以及一个负载均衡和一个THHP缓存，是一个性能极高的服务器
> 
>   ![mark](http://system.51huani.com/printscreen/20190111/N4mYotGEv9p5.png?imageslim)
>
#### 下载安装

> 1. 下载Nginx
>
>    * wget https://nginx.org/download/nginx-1.12.2.tar.gz
>
> 2. 安装依赖库
>
>    * apt-get install make
>    * apt-get install gcc
>    * apt-get install libpcre3 libpcre3-dev
>    * apt-get install zlib1g-dev
>
> 3. 解压Nginx，并在目录中执行
>
>    *  tar zvxf (fileName)
>
>    * ./configure
>
>	![mark](http://system.51huani.com/printscreen/20190111/HLrY0aK4Fbtn.png?imageslim)
>
> 4. 编译并安装
>
>    * make
>
>    ![mark](http://system.51huani.com/printscreen/20190111/KKe1KF3t5FWi.png?imageslim)
>
>    * nake install
>
> 5. 安装后目录在/usr/local/nginx
>
>    ![mark](http://system.51huani.com/printscreen/20190111/0kSAHFun1lSW.png?imageslim)

#### 启动运行

> 1. 启动Nginx
>    * /usr/local/nginx/sbin/nginx
> 2. 重新加载配置文件
>    * /usr/local/nginx/sbin/nginx -s reload
> 3. 重启
>    * /usr/local/nginx/sbin/nginx -s reopen
> 4. 停止
>    * /usr/local/nginx/sbin/nginx -s stop
> 5. 检测配置文件语法是否存在错误
>    * /usr/local/nginx/sbin/nginx -t

#### 配置文件

> 1. 