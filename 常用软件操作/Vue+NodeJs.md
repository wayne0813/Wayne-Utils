### Vue+NodeJs

> #### MVVM
>
> 1. MVVM（Model–view–viewmodel）是一种软件架构模式
> 2. 使用ViewModel对象将数据和视图进行双向绑定。数据的变化会体现在视图中，视图对数据的操作也会更改数据
>
> ![mark](http://system.51huani.com/printscreen/20190115/l2WotisKJ653.png?imageslim)
>
> #### Vue
>
> Vue.js (读音 /vjuː/，类似于 view) 是一套构建用户界面的渐进式框架。与其他重量级框架不同的是，Vue 采用自底向上增量开发的设计。Vue 的核心库只关注视图层，它不仅易于上手，还便于与第三方库或既有项目整合。另一方面，当与单文件组件和 Vue 生态系统支持的库结合使用时，Vue 也完全能够为复杂的单页应用程序提供驱动。
>
> #### Node.js
>
> 1. Node.js® 是一个基于 Chrome V8 引擎的 JavaScript 运行时。 Node.js 使用高效、轻量级的事件驱动、非阻塞I/O 模型。它的包生态系统——npm，是目前世界上最大的开源库生态系统。
>
> 2. 下载
>
>    ![mark](http://system.51huani.com/printscreen/20190115/YGe3wgmbSNdP.png?imageslim)
>
> 3. 安装
>
>    ![mark](http://system.51huani.com/printscreen/20190115/u3CV3E8KcYRo.png?imageslim)
>
> 4. 查看版本
>
>    ![mark](http://system.51huani.com/printscreen/20190115/HOIlPLFGQwLQ.png?imageslim)
>
> 5. 随着Node安装的包依赖组件，类似Java中的Maven，通过命令行npm –v 查看npm的版本
>
> 6. nrm 是一个 NPM 源管理器，允许你快速地在如下 NPM 源间切换
>
>    1. 安装nrm： npm install -g nrm
>
>       ![mark](http://system.51huani.com/printscreen/20190115/xvbRioFWk7d9.png?imageslim)
>
>    2. 查看可用的源： nrm ls
>
>       ![mark](http://system.51huani.com/printscreen/20190115/Ws6jVdxi1Twf.png?imageslim)
>
>    3. 测试源的速度： nrm test
>
>    4. 切换源： nrm use taobao
>
>       ![mark](http://system.51huani.com/printscreen/20190115/bVUux42WYMwk.png?imageslim)
>
> 7. 生成package.json文件
>
> 8. 安装node模块
>
>    1. 全局安装 npm install -g(lobal) 模块名称
>
>       ![mark](http://system.51huani.com/printscreen/20190115/Rgv5q9xXwWeN.png?imageslim)
>
>    2. 本地安装 npm install 模块名称
>
>    3. 安装指定版本号 npm install 模块名称@版本号
>
>    4. 安装运行依赖 npm install 模块名称 -S
>
>    5. 安装开发依赖 npm install 模块名称 -D
>
> 9. 更新模块
>
>    1. 更新本地模块 npm update [模块名称]
>
>    2. 更新全局安装模块 npm update -global [模块名称]
>
> 10. 卸载模块
>
>     1. 卸载安装的本地模块 npm uninstall 模块名称
>
>     2. 卸载全局安装的模块 npm uninstall 模块名称 -global
>
> 11. npm run
>
>     1. npm不仅可以用于模块管理，还可以用于执行脚本。package.json文件有一个scripts字段，可以用于指定脚本命令，供npm直接调用

### Vue项目创建

> * 在执行下述步骤前，默认您已安装Node npm cnpm nrm vue vue-cli
>
> 1. 创建项目
>
>    ~~~ java
>    # 创建一个基于 webpack 模板的新项目
>    λ vue init webpack vue-01				
>    
>    ? Project name vue-01					项目名称 - 默认回车
>    ? Project description A Vue.js project	 项目简介 - 默认回车
>    ? Author Wayne <l_wayne@163.com>		 作者 - 默认回车
>    ? Vue build standalone					回车
>    ? Install vue-router? Yes				建议安装  Y  回车
>    ? Use ESLint to lint your code? No		eslint算是一个代码语法规范检查的工具。 
>    ? Set up unit tests No					建议不安装 N  回车
>    ? Setup e2e tests with Nightwatch? No	  建议不安装 N  回车
>    ? Should we run `npm install` for you after the project has been created? (recommended) npm										选择 NPM
>    
>       vue-cli · Generated "vue-01".
>    
>    
>    # Installing project dependencies ...
>    # ========================
>    
>    ~~~
>
> 2. 创建完成![mark](http://system.51huani.com/notebook/20190410/4AAuUI8PWCec.png?imageslim)
>
> 3. 进入项目 
>
>    ~~~java
>    λ cd vue-01\
>    ~~~
>
> 4. ~~~ java
>    λ cnpm install
>    √ Installed 37 packages
>    √ Linked 0 latest versions
>    √ Run 0 scripts
>    √ All packages installed (used 48ms(network 42ms), speed 0B/s, json 0(0B), tarball 0B)
>    ~~~
>
> 5. 运行
>     ~~~ja
>        λ cnpm run dev
>     ~~~
> 
>      ![mark](http://system.51huani.com/notebook/20190410/TWougvmaHHN5.png?imageslim)
> 
> 6. 功执行以上命令后访问 http://localhost:8080/，输出结果如下所示： ![mark](http://system.51huani.com/notebook/20190410/HKXe9WmNRDdg.png?imageslim)
>    

### Vue项目结构

> ##### 项目结构如下
>
> ![mark](http://system.51huani.com/notebook/20190410/cMfj5NuH1a8U.png?imageslim)
>
> 1. build `build`目录是一些webpack的文件，配置参数等，一般不用修改  
> 2. config `config`是vue项目的基本配置文件 
> 3. node_modules `node_modules`是项目中安装的依赖模块 
> 4. src `src`源码文件夹，自己编写的文件都应该放在这里。 
>    * `assets` 资源文件夹，里面放一些静态资源  
>    * `components`这里放的都是各个组件文件  
>    * `App.vue` App.vue组件
>    * `main.js`入口文件 
> 5. static `static`生成好的文件会放在这个目录下。  
> 6. `.babelrc` babel编译参数，vue开发需要babel编译  
> 7. `.editorconfig` 待补充、、、  
> 8. `.gitignore` 用来过滤一些版本控制的文件，比如node_modules文件夹  
> 9. `index.html` 主页  
> 10. `package.json` 项目文件，记载着一些命令和依赖还有简要的项目描述信息  
> 11. `README.md` 项目介绍











