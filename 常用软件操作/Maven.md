### Maven

1. 下载XXX.rar压缩包

2. 解压

3. 修改本地仓库位置

   1. 打开~/apache-maven-3.5.3/conf/settings.xml文件
   2. 在第55行添加: 

   ~~~xml
   <localRepository>E:/maven/repo</localRepository>
   ~~~

4. 更改中央仓库为阿里云镜像

   1. 在第159行添加:

   ~~~xml
   <mirror>
       <id>aliyun public</id>
       <mirrorOf>central</mirrorOf>
       <name>Human Readable Name for this Mirror.</name>
       <url>http://maven.aliyun.com/nexus/content/groups/public/</url>
   </mirror>
   <mirror>
       <id>aliyun 3rd part</id>
       <mirrorOf>central</mirrorOf>
       <name>Human Readable Name for this Mirror.</name>
       <url>http://maven.aliyun.com/nexus/content/repositories/thirdparty/</url>
   </mirror>
   <mirror>
       <id>aliyun snapshots</id>
       <mirrorOf>central</mirrorOf>
       <name>Human Readable Name for this Mirror.</name>
       <url>http://maven.aliyun.com/nexus/content/repositories/apache-snapshots/</url>
   </mirror>
   <mirror>
       <id>aliyun central</id>
       <mirrorOf>central</mirrorOf>
       <name>Human Readable Name for this Mirror.</name>
       <url>http://maven.aliyun.com/nexus/content/repositories/central/</url>
   </mirror>
   ~~~